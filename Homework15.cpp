﻿

#include <iostream>
#include <string>

int main()
{
    {
        const int N = 16;
        std::cout << ("Even numbres from 0 to") << " " << N << ":\n";
        for (int i = 0; i <= N; i++)
        {
            if (i % 2 <= 0)
            {
                std::cout << i << '\n';
            }
        }
    }

    std::cout << ("Enter N:") << " "; 
    int N;
    std::cin >> N;

    std::cout << ("Even or odd?") << " ";
    std::string TypeOfNumbers;
    std::cin >> TypeOfNumbers;

    for (int i = 0; i <= N; i++)
    {
        float Rem = (i % 2);
        if (TypeOfNumbers == "even" && Rem <= 0 || TypeOfNumbers == "odd" && Rem > 0)
        {
            std::cout << i << '\n';
        }
    }
}